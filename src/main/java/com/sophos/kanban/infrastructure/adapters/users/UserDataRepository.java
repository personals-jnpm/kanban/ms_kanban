package com.sophos.kanban.infrastructure.adapters.users;

import com.sophos.kanban.domain.models.users.User;
import com.sophos.kanban.domain.models.users.gateway.UserRepository;
import com.sophos.kanban.infrastructure.adapters.mappers.UserMapper;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;

import java.util.Optional;

@RequiredArgsConstructor
public class UserDataRepository implements UserRepository {

    private final UserDataJPA repositoryJPA;
    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    @Override
    public Optional<User> findByUsername(String username) {
        return repositoryJPA.findByUsername(username)
                .map(userMapper::dataToEntity);
    }

}
