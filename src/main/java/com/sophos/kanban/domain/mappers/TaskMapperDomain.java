package com.sophos.kanban.domain.mappers;

import com.sophos.kanban.domain.models.tasks.Task;
import com.sophos.kanban.domain.models.users.User;
import com.sophos.kanban.domain.usecases.dtos.requests.TaskSaveRequest;
import com.sophos.kanban.domain.usecases.dtos.responses.StateResponse;
import com.sophos.kanban.domain.usecases.dtos.responses.TaskResponse;

import java.util.List;
import java.util.stream.Collectors;

public class TaskMapperDomain {

    private TaskMapperDomain() {

    }

    public static Task requestToEntity(TaskSaveRequest entity, StateResponse state, User user) {
        Task task = new Task();
        task.setTitle(entity.getTaskName());
        task.setDescription(entity.getDescription());
        task.setState(StateMapperDomain.responseToEntity(state));
        task.setUser(user);
        return task;
    }

    public static TaskResponse entityToResponse(Task entity) {
        TaskResponse response = new TaskResponse();
        response.setId(entity.getTaskId());
        response.setTaskName(entity.getTitle());
        response.setDescription(entity.getDescription());
        response.setStateTask(entity.getState().getName());
        response.setUpdateDate(entity.getUpdateAt());
        return response;
    }

    public static Task addTaskObjects(Task entity, StateResponse state, User user) {
        entity.setState(StateMapperDomain.responseToEntity(state));
        entity.setUser(user);
        return entity;
    }

    public static List<TaskResponse> entitiesToResponse(List<Task> entities) {
        return entities.stream().map(TaskMapperDomain::entityToResponse)
                .collect(Collectors.toList());
    }

}
