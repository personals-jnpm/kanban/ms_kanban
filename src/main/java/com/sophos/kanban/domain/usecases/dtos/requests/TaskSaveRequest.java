package com.sophos.kanban.domain.usecases.dtos.requests;

import lombok.Data;

@Data
public class TaskSaveRequest {

    private String taskName;
    private String description;
    private Integer state;

}
