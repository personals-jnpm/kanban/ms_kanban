package com.sophos.kanban.infrastructure.entries.tasks;

import com.sophos.kanban.application.security.JwtTokenProvider;
import com.sophos.kanban.domain.usecases.dtos.requests.TaskSaveRequest;
import com.sophos.kanban.domain.usecases.dtos.responses.TaskResponse;
import com.sophos.kanban.domain.usecases.tasks.TaskUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.sophos.kanban.application.utils.SecurityConstants.TOKEN_PREFIX;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequiredArgsConstructor
@RequestMapping("/board")
@Validated
public class TaskController {

    private final TaskUseCase taskUseCase;
    private final JwtTokenProvider jwtTokenProvider;

    @PostMapping("")
    public ResponseEntity<TaskResponse> saveTask(@Valid @RequestBody TaskSaveRequest request,
                                                 @RequestHeader(AUTHORIZATION) String token) {
        String username = jwtTokenProvider.getSubject(token.substring(TOKEN_PREFIX.length()));
        return ResponseEntity.ok(taskUseCase.saveTask(request, username));
    }

    @PutMapping("/{id}")
    public ResponseEntity<TaskResponse> updateTask(@PathVariable Integer id,
                                                   @Valid @RequestBody TaskSaveRequest request,
                                                   @RequestHeader(AUTHORIZATION) String token) {
        String username = jwtTokenProvider.getSubject(token.substring(TOKEN_PREFIX.length()));
        return ResponseEntity.ok(taskUseCase.updateTask(request, id, username));
    }

    @PutMapping("/{id}/changeState/{state}")
    public ResponseEntity<TaskResponse> changeState(@PathVariable Integer id,
                                                    @PathVariable Integer state,
                                                    @RequestHeader(AUTHORIZATION) String token) {
        String username = jwtTokenProvider.getSubject(token.substring(TOKEN_PREFIX.length()));
        return ResponseEntity.ok(taskUseCase.changeState(id, state, username));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteTask(@PathVariable Integer id,
                                              @RequestHeader(AUTHORIZATION) String token) {
        String username = jwtTokenProvider.getSubject(token.substring(TOKEN_PREFIX.length()));
        taskUseCase.deleteTask(id, username);
        return ResponseEntity.ok(true);
    }

    @GetMapping("")
    public ResponseEntity<List<TaskResponse>> findAllTasks(@RequestHeader(AUTHORIZATION) String token) {
        String username = jwtTokenProvider.getSubject(token.substring(TOKEN_PREFIX.length()));
        return ResponseEntity.ok(taskUseCase.findAllByUser(username));
    }

}
