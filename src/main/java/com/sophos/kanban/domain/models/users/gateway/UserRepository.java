package com.sophos.kanban.domain.models.users.gateway;

import com.sophos.kanban.domain.models.users.User;

import java.util.Optional;

public interface UserRepository {

    Optional<User> findByUsername(String username);

}
