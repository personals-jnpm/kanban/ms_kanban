package com.sophos.kanban.infrastructure.adapters.mappers;

import com.sophos.kanban.domain.models.tasks.State;
import com.sophos.kanban.infrastructure.adapters.tasks.StateData;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface StateMapper {

    State dataToEntity(StateData stateData);

    List<State> listDataToEntity(List<StateData> dataList);

}
