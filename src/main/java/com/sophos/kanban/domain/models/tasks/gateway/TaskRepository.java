package com.sophos.kanban.domain.models.tasks.gateway;

import com.sophos.kanban.domain.models.tasks.Task;

import java.util.List;
import java.util.Optional;

public interface TaskRepository {

    Task save(Task task);

    void delete(Task task);

    Optional<Task> findById(Integer id);

    List<Task> findAllByUser(String username);

    boolean existTitleInBoard(String title);

}
