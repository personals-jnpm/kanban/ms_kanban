package com.sophos.kanban.domain.usecases.users;

import com.sophos.kanban.domain.models.users.User;

public interface UserUseCase {

    User findUserByUsername(String username);

}
