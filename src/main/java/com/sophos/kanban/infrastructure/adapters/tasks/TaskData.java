package com.sophos.kanban.infrastructure.adapters.tasks;


import com.sophos.kanban.infrastructure.adapters.users.UserData;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "tasks")
@Data
public class TaskData implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer taskId;

    @Column(nullable = false)
    @Size(min = 2)
    private String title;

    @Column(nullable = false)
    @Size(min = 5)
    private String description;

    @ManyToOne()
    @JoinColumn(name = "state_id")
    private StateData state;

    @ManyToOne()
    @JoinColumn(name = "user_id")
    private UserData user;

    @Temporal(TemporalType.DATE)
    private Date createAt;

    @Temporal(TemporalType.DATE)
    private Date updateAt;

    @PrePersist
    public void prePersist() {
        createAt = new Date();
        updateAt = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        updateAt = new Date();
    }

}