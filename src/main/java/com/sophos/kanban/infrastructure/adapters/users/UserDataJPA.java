package com.sophos.kanban.infrastructure.adapters.users;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserDataJPA extends JpaRepository<UserData, Long> {

    Optional<UserData> findByUsername(String username);

}
