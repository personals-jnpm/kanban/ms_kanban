package com.sophos.kanban.domain.models.tasks;

import com.sophos.kanban.domain.models.users.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class Task {

    private Integer taskId;
    private String title;
    private String description;
    private State state;
    private User user;
    private Date updateAt;

    public Task(Integer taskId) {
        this.taskId = taskId;
    }
}
