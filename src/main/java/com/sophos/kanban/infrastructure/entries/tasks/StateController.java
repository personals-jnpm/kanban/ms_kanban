package com.sophos.kanban.infrastructure.entries.tasks;

import com.sophos.kanban.domain.usecases.dtos.responses.StateResponse;
import com.sophos.kanban.domain.usecases.tasks.StateUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/state")
@Validated
public class StateController {

    private final StateUseCase stateUseCase;

    @GetMapping("/{id}")
    public StateResponse findStateById(@PathVariable Integer id) {
        return stateUseCase.findById(id);
    }

    @GetMapping("")
    public List<StateResponse> findAllStates() {
        return stateUseCase.findAll();
    }

}
