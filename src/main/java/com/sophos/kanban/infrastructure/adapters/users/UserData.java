package com.sophos.kanban.infrastructure.adapters.users;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "user_auth")
@Data
public class UserData implements Serializable {
    @Id
    @Column(name = "id", nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String userId;

    private String firstName;

    private String lastName;

    private String username;

    private String password;

    private String email;

    private String profileImageURL;

    private boolean isActive;

    private boolean isNotLocked;

    @Column(name = "role_id")
    private Integer role;

    @Column(name = "last_login")
    private LocalDateTime lastLogin;

    @Column(name = "last_login_display")
    private LocalDateTime lastLoginDisplay;

    @Column(name = "join_date")
    private LocalDateTime joinDate;

}
