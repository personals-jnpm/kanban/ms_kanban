package com.sophos.kanban.infrastructure.adapters.tasks;

import com.sophos.kanban.domain.models.tasks.State;
import com.sophos.kanban.domain.models.tasks.gateway.StateRepository;
import com.sophos.kanban.infrastructure.adapters.mappers.StateMapper;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class StateDataRepository implements StateRepository {

    private final StateDataJPA repositoryJPA;
    private final StateMapper stateMapper = Mappers.getMapper(StateMapper.class);

    @Override
    public Optional<State> findById(Integer id) {
        return repositoryJPA.findById(id)
                .map(stateMapper::dataToEntity);
    }

    @Override
    public List<State> findAll() {
        return stateMapper.listDataToEntity(
                repositoryJPA.findAll()
        );
    }
}
