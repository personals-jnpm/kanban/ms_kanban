package com.sophos.kanban.domain.usecases.tasks;

import com.sophos.kanban.domain.usecases.dtos.requests.TaskSaveRequest;
import com.sophos.kanban.domain.usecases.dtos.responses.TaskResponse;

import java.util.List;

public interface TaskUseCase {

    TaskResponse saveTask(TaskSaveRequest saveRequest, String username);

    TaskResponse updateTask(TaskSaveRequest saveRequest, Integer id, String username);

    TaskResponse changeState(Integer id, Integer state, String username);

    void deleteTask(Integer id, String username);

    List<TaskResponse> findAllByUser(String username);
}
