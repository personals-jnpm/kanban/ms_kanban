package com.sophos.kanban.infrastructure.adapters.mappers;

import com.sophos.kanban.domain.models.users.User;
import com.sophos.kanban.infrastructure.adapters.users.UserData;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper {

    User dataToEntity(UserData userData);

}
