package com.sophos.kanban.domain.models.tasks;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class State {

    private Integer stateId;
    private String name;

}
