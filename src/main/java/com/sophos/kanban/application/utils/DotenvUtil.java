package com.sophos.kanban.application.utils;

import io.github.cdimascio.dotenv.Dotenv;

public class DotenvUtil {

    private static DotenvUtil dotEnvUtil;
    private final Dotenv dotenv;

    private DotenvUtil() {
        dotenv = Dotenv.configure()
                .directory("./")
                .ignoreIfMalformed()
                .filename(".env")
                .ignoreIfMissing()
                .load();
    }

    public static Dotenv getInstance() {
        if (dotEnvUtil == null) {
            dotEnvUtil = new DotenvUtil();
        }
        return dotEnvUtil.getDotenv();
    }

    private Dotenv getDotenv() {
        return dotenv;
    }

    public static String get(String key) {
        return DotenvUtil.getInstance().get(key);
    }

    public static boolean isEqual(String key, String compare) {
        String value = get(key);
        return value != null && value.equals(compare);
    }

}

