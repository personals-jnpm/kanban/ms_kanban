package com.sophos.kanban.domain.models.tasks.gateway;

import com.sophos.kanban.domain.models.tasks.State;

import java.util.List;
import java.util.Optional;

public interface StateRepository {

    Optional<State> findById(Integer id);

    List<State> findAll();

}
