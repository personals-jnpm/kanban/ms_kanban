package com.sophos.kanban.application.utils;

import java.util.List;

public class Constants {
    public static final boolean SHOW_CREDENTIALS = true;
    public static final String ALLOWED_ORIGINS = "http://localhost:4200";
    public static final List<String> ALLOWED_HEADERS = List.of("Origin", "Access-Control-Allow-Origin", "Content-Type",
            "Accept", "Jwt-Token", "Authorization", "Origin, Accept", "Access-Control-Request-Method",
            "X-Request-With", "Access-Control-Request-Headers", "Access-Control-Allow-Origin");
    public static final List<String> EXPOSED_HEADERS = List.of("Origin", "Content-Type", "Accept", "Jwt-Token",
            "Authorization", "Access-Control-Allow-Origin", "Access-Control-Allow-Credentials");
    public static final List<String> ALLOWED_METHODS = List.of("GET", "POST", "PUT", "DELETE", "OPTIONS");

}