package com.sophos.kanban.domain.usecases.dtos.responses;

import lombok.Data;

@Data
public class StateResponse {

    private Integer id;
    private String stateName;

}
