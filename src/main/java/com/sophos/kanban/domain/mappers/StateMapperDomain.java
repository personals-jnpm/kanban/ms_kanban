package com.sophos.kanban.domain.mappers;

import com.sophos.kanban.domain.models.tasks.State;
import com.sophos.kanban.domain.usecases.dtos.responses.StateResponse;

import java.util.List;
import java.util.stream.Collectors;

public class StateMapperDomain {

    private StateMapperDomain() {

    }

    public static State responseToEntity(StateResponse response) {
        State entity = new State();
        entity.setStateId(response.getId());
        entity.setName(response.getStateName());
        return entity;
    }

    public static StateResponse entityToResponse(State entity) {
        StateResponse response = new StateResponse();
        response.setId(entity.getStateId());
        response.setStateName(entity.getName());
        return response;
    }

    public static List<StateResponse> entitiesToResponse(List<State> entities) {
        return entities.stream().map(StateMapperDomain::entityToResponse)
                .collect(Collectors.toList());
    }

}
