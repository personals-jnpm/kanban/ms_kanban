package com.sophos.kanban.application.config;

import com.sophos.kanban.domain.models.tasks.gateway.StateRepository;
import com.sophos.kanban.domain.models.tasks.gateway.TaskRepository;
import com.sophos.kanban.domain.models.users.gateway.UserRepository;
import com.sophos.kanban.domain.usecases.tasks.StateUseCase;
import com.sophos.kanban.domain.usecases.tasks.StateUseCaseImpl;
import com.sophos.kanban.domain.usecases.tasks.TaskUseCase;
import com.sophos.kanban.domain.usecases.tasks.TaskUseCaseImpl;
import com.sophos.kanban.domain.usecases.users.UserUseCase;
import com.sophos.kanban.domain.usecases.users.UserUseCaseImpl;
import com.sophos.kanban.infrastructure.adapters.tasks.StateDataJPA;
import com.sophos.kanban.infrastructure.adapters.tasks.StateDataRepository;
import com.sophos.kanban.infrastructure.adapters.tasks.TaskDataJPA;
import com.sophos.kanban.infrastructure.adapters.tasks.TaskDataRepository;
import com.sophos.kanban.infrastructure.adapters.users.UserDataJPA;
import com.sophos.kanban.infrastructure.adapters.users.UserDataRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Collections;

import static com.sophos.kanban.application.utils.Constants.*;

@Configuration
public class AppConfig {

    @Bean
    public StateRepository stateRepository(StateDataJPA stateDataJPA) {
        return new StateDataRepository(stateDataJPA);
    }

    @Bean
    public StateUseCase stateUseCase(StateRepository stateRepository) {
        return new StateUseCaseImpl(stateRepository);
    }

    @Bean
    public UserRepository userRepository(UserDataJPA userDataJPA) {
        return new UserDataRepository(userDataJPA);
    }

    @Bean
    public UserUseCase userUseCase(UserRepository userRepository) {
        return new UserUseCaseImpl(userRepository);
    }

    @Bean
    public TaskRepository taskRepository(TaskDataJPA taskDataJPA) {
        return new TaskDataRepository(taskDataJPA);
    }

    @Bean
    public TaskUseCase taskUseCase(StateUseCase stateUseCase, UserUseCase userUseCase,
                                   TaskRepository taskRepository) {
        return new TaskUseCaseImpl(stateUseCase, userUseCase, taskRepository);
    }

    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowCredentials(SHOW_CREDENTIALS);
        corsConfiguration.setAllowedOrigins(Collections.singletonList(ALLOWED_ORIGINS));
        corsConfiguration.setAllowedHeaders(ALLOWED_HEADERS);
        corsConfiguration.setExposedHeaders(EXPOSED_HEADERS);
        corsConfiguration.setAllowedMethods(ALLOWED_METHODS);

        UrlBasedCorsConfigurationSource basedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        basedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsFilter(basedCorsConfigurationSource);
    }


}
