package com.sophos.kanban.application.utils;

public class SecurityConstants {

    public static final long EXPIRATION_TIME = 432_000_000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String JWT_TOKEN_HEADER = "Jwt-Token";
    public static final String ARRAYS_LLC = "Auth api";
    public static final String ARRAYS_ADMINISTRATION = "User Management Portal";
    public static final String AUTHORITIES = "authorities";
    public static final String FORBIDDEN_MESSAGE = "Necesitas iniciar sesión para acceder a este recurso";
    public static final String ACCESS_DENIED_MESSAGE = "No tienes permiso para acceder a este sitio";
    public static final String OPTION_HTTP_METHOD = "OPTIONS";
    public static final String[] PUBLIC_URLS = {};
}
