package com.sophos.kanban.application.utils.secrets;

import com.sophos.kanban.application.utils.DotenvUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class SecretConfig {

    @Bean
    @Profile("!local")
    public SecretDBModel getDbModel() {
        return new SecretDBModel();
    }

    @Bean
    @Profile("local")
    public SecretDBModel getDbModelLocal() {
        return new SecretDBModel(DotenvUtil.get("user"),
                DotenvUtil.get("password"), DotenvUtil.get("host"),
                DotenvUtil.get("port"), DotenvUtil.get("dbName"),
                DotenvUtil.get("engine"));
    }

}
