package com.sophos.kanban.domain.models.users;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@Data
@RequiredArgsConstructor
public class User {

    private Long id;
    private String userId;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String email;
    private String profileImageURL;
    private boolean isActive;
    private boolean isNotLocked;
    private Integer role;
    private LocalDateTime lastLogin;
    private LocalDateTime lastLoginDisplay;
    private LocalDateTime joinDate;
}
