package com.sophos.kanban.infrastructure.adapters.tasks;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StateDataJPA extends JpaRepository<StateData, Integer> {

    Optional<StateData> findById(Integer id);

}
