package com.sophos.kanban.domain.usecases.dtos.responses;

import lombok.Data;

import java.util.Date;

@Data
public class TaskResponse {

    private Integer id;
    private String taskName;
    private String description;
    private String stateTask;
    private Date updateDate;

}
