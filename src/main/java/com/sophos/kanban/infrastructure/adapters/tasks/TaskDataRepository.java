package com.sophos.kanban.infrastructure.adapters.tasks;

import com.sophos.kanban.domain.models.tasks.Task;
import com.sophos.kanban.domain.models.tasks.gateway.TaskRepository;
import com.sophos.kanban.infrastructure.adapters.mappers.TaskMapper;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class TaskDataRepository implements TaskRepository {

    private final TaskDataJPA repositoryJPA;
    private final TaskMapper taskMapper = Mappers.getMapper(TaskMapper.class);

    @Override
    public Task save(Task task) {
        return taskMapper.dataToEntity(
                repositoryJPA.save(taskMapper.entityToData(task))
        );
    }

    @Override
    public void delete(Task task) {
        repositoryJPA.delete(taskMapper.entityToData(task));
    }

    @Override
    public Optional<Task> findById(Integer id) {
        return repositoryJPA.findById(id)
                .map(taskMapper::dataToEntity);
    }

    @Override
    public List<Task> findAllByUser(String username) {
        return taskMapper.listDataToEntity(
                repositoryJPA.findAllByUser_Username(username)
        );
    }

    @Override
    public boolean existTitleInBoard(String title) {
        return repositoryJPA.existsByTitleEquals(title);
    }
}
