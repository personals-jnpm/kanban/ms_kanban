package com.sophos.kanban.domain.usecases.tasks;

import com.sophos.kanban.domain.exceptions.CustomException;
import com.sophos.kanban.domain.exceptions.ResourceNotFoundException;
import com.sophos.kanban.domain.mappers.StateMapperDomain;
import com.sophos.kanban.domain.models.tasks.Task;
import com.sophos.kanban.domain.models.tasks.gateway.TaskRepository;
import com.sophos.kanban.domain.models.users.User;
import com.sophos.kanban.domain.usecases.dtos.requests.TaskSaveRequest;
import com.sophos.kanban.domain.usecases.dtos.responses.StateResponse;
import com.sophos.kanban.domain.usecases.dtos.responses.TaskResponse;
import com.sophos.kanban.domain.usecases.users.UserUseCase;
import lombok.RequiredArgsConstructor;

import java.util.List;

import static com.sophos.kanban.domain.mappers.TaskMapperDomain.*;

@RequiredArgsConstructor
public class TaskUseCaseImpl implements TaskUseCase {
    private final String RESOURCE_NOT_FOUND = "Task with id %s not found";

    private final StateUseCase stateUseCase;
    private final UserUseCase userUseCase;
    private final TaskRepository taskRepository;

    @Override
    public TaskResponse saveTask(TaskSaveRequest saveRequest, String username) {
        User user = userUseCase.findUserByUsername(username);
        StateResponse state = stateUseCase.findById(saveRequest.getState());
        verifyUniqueTitleTaskInBoard(saveRequest.getTaskName());

        return entityToResponse(
                taskRepository.save(requestToEntity(saveRequest, state, user))
        );
    }

    @Override
    public TaskResponse updateTask(TaskSaveRequest saveRequest, Integer id, String username) {
        User user = userUseCase.findUserByUsername(username);
        Task task = findTaskById(id);
        validateUserOwnership(task.getUser(), user);

        StateResponse state = stateUseCase.findById(saveRequest.getState());

        return entityToResponse(
                taskRepository.save(updateTask(task, saveRequest, state))
        );
    }

    @Override
    public TaskResponse changeState(Integer id, Integer state, String username) {
        User user = userUseCase.findUserByUsername(username);
        Task task = findTaskById(id);
        validateUserOwnership(task.getUser(), user);
        StateResponse stateResponse = stateUseCase.findById(state);

        return entityToResponse(
                taskRepository.save(addTaskObjects(task, stateResponse, user))
        );
    }

    public Task findTaskById(Integer id) {
        return taskRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(RESOURCE_NOT_FOUND, id)));
    }

    @Override
    public void deleteTask(Integer id, String username) {
        User user = userUseCase.findUserByUsername(username);
        validateUserOwnership(findTaskById(id).getUser(), user);
        taskRepository.delete(new Task(id));
    }

    @Override
    public List<TaskResponse> findAllByUser(String username) {
        return entitiesToResponse(
                taskRepository.findAllByUser(username)
        );
    }

    private void verifyUniqueTitleTaskInBoard(String title) {
        if (taskRepository.existTitleInBoard(title)) {
            throw new CustomException(String.format("The task title '%s' already exist in current board", title));
        }
    }

    private void validateUserOwnership(User user, User userRequest) {
        if (!user.isActive() || !user.isNotLocked() || user.getUserId() != userRequest.getUserId()) {
            throw new CustomException(String.format("The user %s is unauthorized to perform this operation",
                    userRequest.getUsername()));
        }
    }

    private Task updateTask(Task task, TaskSaveRequest taskRequest, StateResponse state) {
        task.setState(StateMapperDomain.responseToEntity(state));
        task.setTitle(taskRequest.getTaskName());
        task.setDescription(taskRequest.getDescription());
        return task;
    }

}
