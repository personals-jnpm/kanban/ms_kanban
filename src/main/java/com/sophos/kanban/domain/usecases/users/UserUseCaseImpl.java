package com.sophos.kanban.domain.usecases.users;

import com.sophos.kanban.domain.exceptions.ResourceNotFoundException;
import com.sophos.kanban.domain.models.users.User;
import com.sophos.kanban.domain.models.users.gateway.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@RequiredArgsConstructor
@Slf4j
public class UserUseCaseImpl implements UserUseCase {
    private final String USER_BY_USERNAME_NOT_FOUND = "Username '%s' not found";

    private final UserRepository repository;

    @Override
    public User findUserByUsername(String username) {
        return repository.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(USER_BY_USERNAME_NOT_FOUND, username)));
    }

}
