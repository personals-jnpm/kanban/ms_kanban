package com.sophos.kanban.infrastructure.adapters.tasks;


import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "task_states")
@Data
public class StateData implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer stateId;

    @Column(nullable = false)
    @Size(min = 4)
    private String name;

}