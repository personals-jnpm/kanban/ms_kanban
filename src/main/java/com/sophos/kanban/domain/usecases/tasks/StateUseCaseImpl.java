package com.sophos.kanban.domain.usecases.tasks;

import com.sophos.kanban.domain.exceptions.ResourceNotFoundException;
import com.sophos.kanban.domain.models.tasks.State;
import com.sophos.kanban.domain.models.tasks.gateway.StateRepository;
import com.sophos.kanban.domain.usecases.dtos.responses.StateResponse;
import lombok.RequiredArgsConstructor;

import java.util.List;

import static com.sophos.kanban.domain.mappers.StateMapperDomain.entitiesToResponse;
import static com.sophos.kanban.domain.mappers.StateMapperDomain.entityToResponse;

@RequiredArgsConstructor
public class StateUseCaseImpl implements StateUseCase {
    private final String RESOURCE_NOT_FOUND = "State with id %s not found";

    private final StateRepository stateRepository;

    @Override
    public StateResponse findById(Integer id) {
        State state = stateRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(RESOURCE_NOT_FOUND, id)));
        return entityToResponse(state);
    }

    @Override
    public List<StateResponse> findAll() {
        return entitiesToResponse(stateRepository.findAll());
    }
}
