package com.sophos.kanban.domain.usecases.tasks;

import com.sophos.kanban.domain.usecases.dtos.responses.StateResponse;

import java.util.List;

public interface StateUseCase {

    StateResponse findById(Integer id);

    List<StateResponse> findAll();

}
