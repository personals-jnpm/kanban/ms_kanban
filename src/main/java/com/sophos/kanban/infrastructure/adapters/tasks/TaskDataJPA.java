package com.sophos.kanban.infrastructure.adapters.tasks;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TaskDataJPA extends JpaRepository<TaskData, Integer> {

    Optional<TaskData> findById(@NotNull Integer id);

    List<TaskData> findAllByUser_Username(String username);

    boolean existsByTitleEquals(String title);

}
