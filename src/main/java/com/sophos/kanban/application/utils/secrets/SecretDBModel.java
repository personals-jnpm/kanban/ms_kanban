package com.sophos.kanban.application.utils.secrets;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SecretDBModel {

    private String username;
    private String password;
    private String host;
    private String port;
    private String dbname;
    private String engine;

}
