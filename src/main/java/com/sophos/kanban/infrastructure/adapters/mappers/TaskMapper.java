package com.sophos.kanban.infrastructure.adapters.mappers;

import com.sophos.kanban.domain.models.tasks.Task;
import com.sophos.kanban.infrastructure.adapters.tasks.TaskData;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface TaskMapper {

    Task dataToEntity(TaskData taskData);

    List<Task> listDataToEntity(List<TaskData> dataList);

    TaskData entityToData(Task taskData);

    List<TaskData> listEntityToData(List<Task> dataList);

}
